package AssignmentRestaurantBookingSystem;


import java.util.Scanner;

public class assignmentRestaurantBookingSystem {

//TODO make basket function, make checkout, display customer name seat number and purchases
	
	public static void main(String[] args) {
		
		
		String RestaurantBooking [][]= new String[6][7];
		Scanner input = new Scanner(System.in);
		String CustomerName, ItemCode;
		int Bookingchoice= 0,SeatNumber = 0, BasketChoice = 0, ItemChoice=0, ItemAmount = 0, BasketCost=0, AmountPayed=0, Change = 0; 
		boolean Noavailableseating=false,seatbooked=false, ItemsInBasket = false, CostNotCovered = true;
		boolean Tablebookingloopactive = true, Basketloopactive = true;
		int ArrayFilling = 01;
		//creates objects
		FoodAndDrink objPizza = new FoodAndDrink();
		FoodAndDrink objSteak = new FoodAndDrink();
		FoodAndDrink objSandwich = new FoodAndDrink();
		FoodAndDrink objWater = new FoodAndDrink();
		FoodAndDrink objSoftDrink = new FoodAndDrink();
		FoodAndDrink objTea = new FoodAndDrink();
		FoodAndDrink objCoffee = new FoodAndDrink();
		FoodAndDrink objIceCream = new FoodAndDrink();
		FoodAndDrink objChocolate = new FoodAndDrink();
		//sets properties of objects
		objPizza.pizzasetting();
		objSteak.steaksetting();
		objSandwich.sandwichsetting();
		objWater.watersetting();
		objSoftDrink.softdrinksetting();
		objTea.teasetting();
		objCoffee.coffeesetting();
		objIceCream.icecreamsetting();
		objChocolate.chocolatesetting();
		
		//Initialises all values in the array to the appropriate seating number.
		for(int i = 0; i < RestaurantBooking.length; i++) {
			for(int j = 0; j < RestaurantBooking[i].length; j++) {
				if (ArrayFilling <10) {
					//converts the int value into a string to store in the array.
					RestaurantBooking [i][j] = "0" + String.valueOf(ArrayFilling++);
				}
				else {
					RestaurantBooking [i][j] = String.valueOf(ArrayFilling++);
				}
				
			}
		
		}
		
		System.out.println("Please enter your name.");
		//Allows user to input their name.
		CustomerName = input.nextLine();
		/*This runs a function to check if any seats are available, if there aren't it sets the variable,
		 * Noavailableseating, to true. Otherwise it sets the variable to false.*/
		Noavailableseating = availableseating(RestaurantBooking);
		//This checks the variable to see if it is true or false
		if(Noavailableseating == true){
			System.out.println("Welcome " + CustomerName + ".\nSorry there is no available seat at that time");
			return;
		}
		//dowhile loop causes code to repeat as long as Tablebookingloopactive is true, 
		do {
			
			System.out.println("Welcome " + CustomerName + ".\nDo You wish to;");
			System.out.println("1:Automatically Book A Table");
			System.out.println("2:Manually");
			//Allows user to input their choice;
			Bookingchoice = Integer.valueOf(input.nextLine());
			/*switch allow user to choose one of two options, any other value input runs the default 
			 * path which causes an error message to appear and allows the loop to repeat*/
			switch(Bookingchoice) {
				//Case 1 causes the array to be searched for the first available element not equal to XX.
				case 1:
				{
					
					for(int i = 0; i < RestaurantBooking.length; i++) {
						for(int j = 0; j< RestaurantBooking[i].length; j++) {
							//Checks array for first element not equal to XX.
							if(RestaurantBooking[i][j] == "XX") {
								continue;
							}
							else if (RestaurantBooking[i][j] !="XX")
							{
								//turns string value into integer
								SeatNumber = Integer.valueOf(RestaurantBooking[i][j]);
								RestaurantBooking[i][j] = "XX";
								seatbooked = true;
								break;
							}
							//breaks loop if bool is true.
							if(seatbooked == true)
							{break;
							}
						}
						if(seatbooked == true)
						{break;
						}
					}
					display(RestaurantBooking);
					Tablebookingloopactive = false;
					break;
				}
					//Case 2 took me almost 5 hours to get working
				case 2:{
					display(RestaurantBooking);
					System.out.println("Which seat do you wish to book, Seats labeled XX are already booked");
					
					SeatNumber = Integer.valueOf(input.nextLine());
					System.out.println(SeatNumber);
					for(int i = 0; i <6;i++) {
						
						for(int j = 0;j < 7;j++) {
							/*this if statement is the cause of my suffering, why does it care that the string value
							 * is made into an integer rather the integer value being turned into a string.*/
							if(Integer.valueOf(RestaurantBooking[i][j])==SeatNumber) {
								System.out.println("Please");
								
								RestaurantBooking[i][j]="XX";
								seatbooked = true;
							}
								if(seatbooked == true){
									break;	
								}
							if(seatbooked == true){
								break;
							}
						}
					}
					//calls method display and sets Tablebookingloopactive to false to as to stop the loop
					display(RestaurantBooking);
					Tablebookingloopactive = false;
					break;
				}
				default:{
						System.out.println("You have selected an invalid option, please try again");
						break;
				}
			}
		}while(Tablebookingloopactive == true);
	//dowhile loop to display the basket interface as long as options 3 and 4 arent chosen
		do {
			System.out.println("Do you wish to:\n1:Add item to your basket.\n2:Remove item from your basket.\n3:Proceed to checkout"
					+ "\n4:Exit");
			//turns the input string into integer value
			BasketChoice = Integer.valueOf(input.nextLine());
			switch(BasketChoice) {
			case 1:{
				System.out.println("Please enter the code of the item you wish to be added to the basket.");
				System.out.print("----------------------------------\n" +objPizza.name + "\nCode:" + objPizza.code
						+ "\nPrice:�" + objPizza.price + "\nQuantity:" + objPizza.quantity + "\n\n");
				System.out.print(objSteak.name + "\nCode:" + objSteak.code
						+ "\nPrice:�" + objSteak.price + "\nQuantity:" + objSteak.quantity + "\n\n");
				System.out.print(objSandwich.name + "\nCode:" + objSandwich.code
						+ "\nPrice:�" + objSandwich.price + "\nQuantity:" + objSandwich.quantity + "\n\n");
				System.out.print(objWater.name + "\nCode:" + objWater.code
						+ "\nPrice:�" + objWater.price + "\nQuantity:" + objWater.quantity + "\n\n");
				System.out.print(objSoftDrink.name + "\nCode:" + objSoftDrink.code
						+ "\nPrice:�" + objSoftDrink.price + "\nQuantity:" + objSoftDrink.quantity + "\n\n");
				System.out.print(objTea.name + "\nCode:" + objTea.code
						+ "\nPrice:�" + objTea.price + "\nQuantity:" + objTea.quantity + "\n\n");
				System.out.print(objCoffee.name + "\nCode:" + objCoffee.code
						+ "\nPrice:�" + objCoffee.price + "\nQuantity:" + objCoffee.quantity + "\n\n");
				System.out.print(objIceCream.name + "\nCode:" + objIceCream.code
						+ "\nPrice:�" + objIceCream.price + "\nQuantity:" + objIceCream.quantity + "\n\n");
				System.out.print(objChocolate.name + "\nCode:" + objChocolate.code
						+ "\nPrice:�" + objChocolate.price + "\nQuantity:" + objChocolate.quantity + "\n\n");
				System.out.print("----------------------------------\n");
				System.out.print("The current total cost of all items in the basket is:�" + BasketCost + "\n");
				//input to control the add item nested switch
				ItemCode=input.nextLine();
				System.out.println("Please enter the amount of this item you wish to add");
				//added before the switch so i only have to write the statement once rather then in every case.
				ItemAmount=Integer.valueOf(input.nextLine());
				switch(ItemCode) {
				/*each case adds the user dictated amount of the selected item to the basketquantity
				 * property and takes away the appropriate amount from the quantity property by calling
				 * additemBasket function from class FoodAndDrink*/
					case "01":{
						objPizza.additemBasket(ItemAmount);
						BasketCost = BasketCost + (ItemAmount * objPizza.price);
						ItemsInBasket = true;
						break;
					}
					case "02":{
						objSteak.additemBasket(ItemAmount);
						BasketCost = BasketCost + (ItemAmount * objSteak.price);
						ItemsInBasket = true;
						break;			
					}
					case "03":{
						objSandwich.additemBasket(ItemAmount);
						BasketCost = BasketCost + (ItemAmount * objSandwich.price);
						ItemsInBasket = true;
						break;
					}
					case "04":{
						objWater.additemBasket(ItemAmount);
						BasketCost = BasketCost + (ItemAmount * objWater.price);
						ItemsInBasket = true;
						break;
					}
					case "05":{
						objSoftDrink.additemBasket(ItemAmount);
						BasketCost = BasketCost + (ItemAmount * objSoftDrink.price);
						ItemsInBasket = true;
						break;
					}
					case "06":{
						objTea.additemBasket(ItemAmount);
						BasketCost = BasketCost + (ItemAmount * objTea.price);
						ItemsInBasket = true;
						break;
					}
					case "07":{
						objCoffee.additemBasket(ItemAmount);
						BasketCost = BasketCost + (ItemAmount * objCoffee.price);
						ItemsInBasket = true;
						break;
					}
					case "08":{
						objIceCream.additemBasket(ItemAmount);
						BasketCost = BasketCost + (ItemAmount * objIceCream.price);
						ItemsInBasket = true;
						break;
					}
					case "09":{
						objChocolate.additemBasket(ItemAmount);
						BasketCost = BasketCost + (ItemAmount * objChocolate.price);
						ItemsInBasket = true;
						break;
					}
					default:{
						System.out.println("You have entered an invalid Item code. Please try again");
						break;
					}
					
				}
				System.out.print("The total cost of all items in the basket is:�" + BasketCost + "\n");
				break;
			}
			case 2:{
				//Displays list of items in the basket if there are items in the basket
				if(ItemsInBasket == true) {
					System.out.println("Please enter the code of the item you wish to remove from the basket");
					System.out.print("----------------------------------\n");
					//makes it so only items in the basket appear
					if(objPizza.basketQuantity >= 1) {
						System.out.print(objPizza.name  + "\nCode:" + objPizza.code +  "\nAmount in Basket:" + objPizza.basketQuantity + "\n");
					}
					if(objSteak.basketQuantity >= 1) {
						System.out.print(objSteak.name  + "\nCode:" + objSteak.code +  "\nAmount in Basket:" + objSteak.basketQuantity + "\n");
					}
					if(objSandwich.basketQuantity >= 1) {
						System.out.print(objSandwich.name  + "\nCode:" + objSandwich.code  + "\nAmount in Basket:" + objSandwich.basketQuantity + "\n");
					}
					if(objWater.basketQuantity >= 1) {
						System.out.print(objWater.name  + "\nCode:" + objWater.code + "\nAmount in Basket:" + objWater.basketQuantity + "\n");
					}
					if(objSoftDrink.basketQuantity >= 1) {
						System.out.print(objSoftDrink.name  + "\nCode:" + objSoftDrink.code +  "\nAmount in Basket:" + objSoftDrink.basketQuantity + "\n");
					}
					if(objTea.basketQuantity >= 1) {
						System.out.print(objTea.name  + "\nCode:" + objTea.code + "\nAmount in Basket:" + objTea.basketQuantity + "\n");
					}
					if(objCoffee.basketQuantity >= 1) {
						System.out.print(objCoffee.name  + "\nCode:" + objCoffee.code +  "\nAmount in Basket:" + objCoffee.basketQuantity + "\n");
					}
					if(objIceCream.basketQuantity >= 1) {
						System.out.print(objIceCream.name  + "\nCode:" + objIceCream.code + "\nAmount in Basket:" + objIceCream.basketQuantity + "\n");
					}
					if(objChocolate.basketQuantity >= 1) {
						System.out.print(objChocolate.name  + "\nCode:" + objChocolate.code + "\nAmount in Basket:" + objChocolate.basketQuantity + "\n");
					}
					System.out.print("\n----------------------------------\n");
					ItemCode = input.nextLine();
					System.out.println("How many of the item do you wish to remove");
					ItemAmount = Integer.valueOf(input.nextLine());
				
					switch(ItemCode) {
					/*each case removes the user dictated amount of the selected item from the basketquantity
					 * property and adds the appropriate amount to the quantity property by calling
					 * removeitemBasket function from class FoodAndDrink*/
						case "01":{
							objPizza.removeitemBasket(ItemAmount);
							BasketCost = BasketCost - (ItemAmount * objPizza.price);
							break;
						}
						case "02":{
							objSteak.removeitemBasket(ItemAmount);
							BasketCost = BasketCost - (ItemAmount * objSteak.price);
							break;			
						}
						case "03":{
							objSandwich.removeitemBasket(ItemAmount);
							BasketCost = BasketCost - (ItemAmount * objSandwich.price);
							break;
						}
						case "04":{
							objWater.removeitemBasket(ItemAmount);
							BasketCost = BasketCost - (ItemAmount * objWater.price);
							break;
						}
						case "05":{
							objSoftDrink.removeitemBasket(ItemAmount);
							BasketCost = BasketCost - (ItemAmount * objSoftDrink.price);
							break;
						}
						case "06":{
							objTea.removeitemBasket(ItemAmount);
							BasketCost = BasketCost - (ItemAmount * objTea.price);
							break;
						}
						case "07":{
							objCoffee.removeitemBasket(ItemAmount);
							BasketCost = BasketCost - (ItemAmount * objCoffee.price);
							break;
						}
						case "08":{
							objIceCream.removeitemBasket(ItemAmount);
							BasketCost = BasketCost - (ItemAmount * objIceCream.price);
							break;
						}
						case "09":{
							objChocolate.removeitemBasket(ItemAmount);
							BasketCost = BasketCost - (ItemAmount * objChocolate.price);
							break;
						}
						default:{
							System.out.println("You have entered an invalid Item code. Please try again");
							break;
						}
					}
					System.out.print("The current total cost of all items in the basket is:�" + BasketCost + "\n");
					break;
				}
				
				else if (ItemsInBasket == false) {
					System.out.println("You have no items in the basket");
					break;
				}
				
				break;
			}
			//this is the checkout section
			case 3:{
				
					System.out.println("The cost of this purchase is:�" + BasketCost + "\n Please enter the amount"
							+ " you wish to pay");
					//this inputs the amount the user wishes to pay
					AmountPayed = Integer.valueOf(input.nextLine());
					//this cancels the booking if the amount payed does not cover the bill
					if(AmountPayed < BasketCost){
						System.out.println("The amount you have payed does not cover the total cost of purchase"
							+ "\n You still owe �" + (BasketCost - AmountPayed) + ".\nDue to failure to pay the correct amount,"
									+ " Your booking has been canceled");
					}
					//this makes it so if the amount payed is equal to the bill, it displays no change owed
					else if(AmountPayed == BasketCost){
						System.out.println("You have payed the exact cost of the purchase, no change is owed, Order details below\n");
						System.out.println("Customer name: " + CustomerName);
						System.out.println("Seat Number: " + SeatNumber);
						System.out.println("Items Purchased:");
						if(objPizza.basketQuantity >= 1) {
							System.out.print(objPizza.name + "\nAmount Ordered:" + objPizza.basketQuantity + "\n");
						}
						if(objSteak.basketQuantity >= 1) {
							System.out.print(objSteak.name + "\nAmount Ordered:" + objSteak.basketQuantity + "\n");
						}
						if(objSandwich.basketQuantity >= 1) {
							System.out.print(objSandwich.name + "\nAmount Ordered:" + objSandwich.basketQuantity + "\n");
						}
						if(objWater.basketQuantity >= 1) {
							System.out.print(objWater.name + "\nAmount Ordered:" + objWater.basketQuantity + "\n");
						}
						if(objSoftDrink.basketQuantity >= 1) {
							System.out.print(objSoftDrink.name + "\nAmount Ordered:" + objSoftDrink.basketQuantity + "\n");
						}
						if(objTea.basketQuantity >= 1) {
							System.out.print(objTea.name + "\nAmount Ordered:" + objTea.basketQuantity + "\n");
						}
						if(objCoffee.basketQuantity >= 1) {
							System.out.print(objCoffee.name + "\nAmount Ordered:" + objCoffee.basketQuantity + "\n");
						}
						if(objIceCream.basketQuantity >= 1) {
							System.out.print(objIceCream.name + "\nAmount Ordered:" + objIceCream.basketQuantity + "\n");
						}
						if(objChocolate.basketQuantity >= 1) {
							System.out.print(objChocolate.name + "\nAmount Ordered:" + objChocolate.basketQuantity + "\n");
						}
						System.out.println("Total cost: �" + BasketCost);
						System.out.println("Total paid: �" + AmountPayed);
						System.out.println ("Total change owed: �" + Change);
						
					}
					//this makes it so that if the amount paid is greater then the bill, the amount of change owed is displayed
					else if(AmountPayed>BasketCost) {
						Change = AmountPayed - BasketCost;
						System.out.println("You have payed �" + AmountPayed + ", and are owed �" + Change + ", in change , Order details below\n");
						System.out.println("Customer name: " + CustomerName);
						System.out.println("Seat Number: " + SeatNumber);
						System.out.println("Items Purchased:");
						if(objPizza.basketQuantity >= 1) {
							System.out.print(objPizza.name + "\nAmount Ordered:" + objPizza.basketQuantity + "\n");
						}
						if(objSteak.basketQuantity >= 1) {
							System.out.print(objSteak.name + "\nAmount Ordered:" + objSteak.basketQuantity + "\n");
						}
						if(objSandwich.basketQuantity >= 1) {
							System.out.print(objSandwich.name + "\nAmount Ordered:" + objSandwich.basketQuantity + "\n");
						}
						if(objWater.basketQuantity >= 1) {
							System.out.print(objWater.name + "\nAmount Ordered:" + objWater.basketQuantity + "\n");
						}
						if(objSoftDrink.basketQuantity >= 1) {
							System.out.print(objSoftDrink.name + "\nAmount Ordered:" + objSoftDrink.basketQuantity + "\n");
						}
						if(objTea.basketQuantity >= 1) {
							System.out.print(objTea.name + "\nAmount Ordered:" + objTea.basketQuantity + "\n");
						}
						if(objCoffee.basketQuantity >= 1) {
							System.out.print(objCoffee.name + "\nAmount Ordered:" + objCoffee.basketQuantity + "\n");
						}
						if(objIceCream.basketQuantity >= 1) {
							System.out.print(objIceCream.name + "\nAmount Ordered:" + objIceCream.basketQuantity + "\n");
						}
						if(objChocolate.basketQuantity >= 1) {
							System.out.print(objChocolate.name + "\nAmount Ordered:" + objChocolate.basketQuantity + "\n");
						}
						System.out.println("Total cost: �" + BasketCost);
						System.out.println("Total paid: �" + AmountPayed);
						System.out.println ("Total change owed: �" + Change);
						System.out.println("Please press any key to confirm");
						input.nextLine();
						
					}
				return ;
				
			}
			case 4:{
				//this ends the current run of the code if 4 is input
				System.out.println("You have chosen to exit. Good Bye");
				System.out.println("Please press any key to confirm");
				input.nextLine();
				return;
			}
			default:{
				System.out.println("You have chosen an invalid option, please try again");
				break;
			}
			}
		}while(Basketloopactive = true);
	
	}
		
	//used to display seating, turned into function due to repeated use.

	static void display(String RestaurantBooking[][]) {
		
		for(int i = 0; i < RestaurantBooking.length; i++) {
			for(int j = 0; j < RestaurantBooking[i].length; j++) {
				System.out.print("[" + RestaurantBooking[i][j] + "]");
			}
			System.out.println();
		}
	}
	//availableseating auto checks if
	public static boolean availableseating(String RestaurantBooking[][]) {
		
		boolean noseats=false;
		for(int i = 0; i < RestaurantBooking.length; i++) {
			for(int j = 0; j < RestaurantBooking[i].length; j++) {
				if(RestaurantBooking[i][j] == "XX") {
					noseats = true;
					continue;
				}
				else {
					noseats = false;
					continue;
				}
			}
			
		}
		return noseats;
	}


}	

	
